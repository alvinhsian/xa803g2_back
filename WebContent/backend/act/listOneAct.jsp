<%@ page contentType="text/html; charset=Big5"%>
<%@ page import="com.act.model.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.pow.model.*"%>
<jsp:useBean id="powSvc" scope="page" class="com.pow.model.PowService" />
<%
ActVO actVO = (ActVO) request.getAttribute("actVO"); //EmpServlet.java(Concroller), 存入req的empVO物件
PowVO powVO = powSvc.getOnePowByPKs((Integer)session.getAttribute("empNo"), 4001);
List<PowVO> listPower = (List<PowVO>)session.getAttribute("list");

%>
<html>
<head>
<title>活動資料 - listOneEmp.jsp</title>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/style.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<script src="<%=request.getContextPath()%>/menu.js"></script>	
<title>前端-活動新增 - addAct.jsp</title>

</head>
<body bgcolor='white'>
			<%@ include file="/menu1.jsp" %> 
<c:if test="<%=listPower.contains(powVO)%>"> 

<table border='1' cellpadding='5' cellspacing='0' width='600'>
	<tr bgcolor='#CCCCFF' align='center' valign='middle' height='20'>
		<td>
		<h3>活動資料 - listOneEmp.jsp</h3>
		<a href="select_page.jsp"><img src="images/back1.gif" width="100" height="32" border="0">回首頁</a>
		</td>
	</tr>
</table>

<table border='1' bordercolor='#CCCCFF' width='600'>
	<tr>
		<th>活動編號</th>
		<th>活動名稱</th>
		<th>活動內容</th>
		<th>開始時間</th>
		<th>結束時間</th>
		<th>活動照片</th>
		<th>租借器材</th>
		<th>押金費用</th>
		<th>主辦方繳交費用</th>
		<th>活動報名費用</th>
		<th>目前活動狀態</th>
		<th>主辦方會員編號</th>
		<th>審核職員編號</th>
	</tr>
	<tr align='center' valign='middle'>
			<td>${actVO.actNo}</td>
			<td>${actVO.actName}</td>
			<td>${actVO.actContent}</td>
			<td>${actVO.actStartTime}</td>
			<td>${actVO.actEndTime}</td>
			<td><img src="DBGifReader3?actNo=${actVO.actNo}"></td>
			<td>${actVO.actEquipment}</td>
			<td>${actVO.actDeposit}</td>
			<td>${actVO.actHostFee}</td>
			<td>${actVO.actRegFee}</td>
			<td>${actVO.actStatus}</td>
			<td>${actVO.memNo}</td>
			<td>${actVO.empNo}</td>
	</tr>
</table>
</c:if>
			<%@ include file="/menu2.jsp" %> 

</body>
</html>
