<%@ page contentType="text/html; charset=Big5"%>
<%@ page import="com.emp.model.*"%>

<%@ page import="java.util.*"%>
<%@ page import="com.pow.model.*"%>
<jsp:useBean id="powSvc" scope="page" class="com.pow.model.PowService" />
<%
EmployeeVO employeeVO = (EmployeeVO) request.getAttribute("employeeVO");

PowVO powVO = powSvc.getOnePowByPKs((Integer)session.getAttribute("empNo"), 4009);
List<PowVO> listPower = (List<PowVO>)session.getAttribute("list");
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/style.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<script src="<%=request.getContextPath()%>/menu.js"></script>	

<title>員工資料 - listOneEmp.jsp</title>
</head>
<body bgcolor='white'>
<c:if test="<%=listPower.contains(powVO)%>"> 
	<%@ include file="/menu1.jsp" %> 
<table border='1' cellpadding='5' cellspacing='0' width='600'>
	<tr bgcolor='#CCCCFF' align='center' valign='middle' height='20'>
		<td>
		<h3>員工資料 - ListOneEmp.jsp</h3>
		<a href="select_page.jsp"><img src="<%=request.getContextPath()%>/emp/images/back1.gif" width="100" height="32" border="0">回首頁</a>
		</td>
	</tr>
</table>

<table border='1' bordercolor='#CCCCFF' width='600'>
	<tr>
		<th>員工編號</th>
		<th>員工姓名</th>
		<th>出生年月日</th>
		<th>電話</th>
		<th>性別</th>
		<th>職稱</th>
		<th>薪資</th>
		<th>到職日</th>
		<th>離職日</th>
		<th>身分證字號</th>
		<th>住址</th>
		<th>照片</th>
	</tr>
	<tr align='center' valign='middle'>
		<td><%=employeeVO.getEmpNo()%></td>
		<td><%=employeeVO.getEmpName()%></td>
		<td><%=employeeVO.getEmpBirth()%></td>
		<td><%=employeeVO.getEmpTel()%></td>
		<td><%=employeeVO.getEmpSex()%></td>
		<td><%=employeeVO.getEmpPos()%></td>
		<td><%=employeeVO.getEmpSalary()%></td>
		<td><%=employeeVO.getEmpArrDate()%></td>
		<td><%=employeeVO.getEmpOff()%></td>
		<td><%=employeeVO.getEmpID()%></td>
		<td><%=employeeVO.getEmpAdd()%></td>
		<td><%=employeeVO.getEmpPic()%></td>
	</tr>
</table>

</c:if> 
	<%@ include file="/menu2.jsp" %> 

</body>
</html>
