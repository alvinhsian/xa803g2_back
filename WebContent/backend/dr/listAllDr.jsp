<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.doctor.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%@ page import="com.pow.model.*"%>
<jsp:useBean id="powSvc" scope="page" class="com.pow.model.PowService" />

<%
	DoctorService drSvc = new DoctorService();
	List<DoctorVO> list = drSvc.getAll();
	pageContext.setAttribute("list", list);
	

	PowVO powVO = powSvc.getOnePowByPKs((Integer)session.getAttribute("empNo"), 4004);
	List<PowVO> listPower = (List<PowVO>)session.getAttribute("list");
%>

<html>
<head>
<title>所有醫師資料 - listAllDr.jsp</title>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/style.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<script src="<%=request.getContextPath()%>/menu.js"></script>		

</head>
<body bgcolor='white'>
<%@ include file="/menu1.jsp" %> 
<c:if test="<%=listPower.contains(powVO)%>"> 
<!--     <h2>動物留言管理</h2> -->
<!--     <div id="search_bar"> -->
<!--       <select name="xxx"> -->
<!--         <option value="111">111</option> -->
<!--         <option value="222">222</option> -->
<!--         <option value="333">333</option> -->
<!--       </select> -->
<!--       <input type="text" name="zzz" id="zzz" /> -->
<!--       <a href="#" class="btn">搜尋</a> </div> -->
    <!-- end #search_bar -->
<!-- jsp開始 -->
	<b><font color=red>此頁練習採用 EL 的寫法取值:</font></b>
	<table border='1' cellpadding='5' cellspacing='0' width='800'>
		<tr bgcolor='#CCCCFF' align='center' valign='middle' height='20'>
			<td>
				<h3>所有醫師資料 - ListAllDr.jsp</h3> 
				<a href="<%=request.getContextPath()%>/backend/doctor-mo.jsp">
				<img src="images/back1.gif" width="100" height="32" border="0">回首頁</a>
			</td>
		</tr>
	</table>

	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font color='red'>請修正以下錯誤:
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>

	<table border='1' bordercolor='#CCCCFF' width='800'>
		<tr>
			<th>醫師編號</th>
			<th>醫師姓名</th>
			<th>學經歷</th>
			<th>性別</th>
			<th>照片</th>
			<th>出生年月日</th>
			<th>住址</th>
			<th>電話</th>
			<th>修改</th>
			<th>刪除</th>
		</tr>

		<%@ include file="pages/page1.file"%>
		<c:forEach var="doctorVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
			<tr align='center' valign='middle' ${(doctorVO.drNo==param.drNo) ? 'bgcolor=#CCCCFF':''}>
				<td>${doctorVO.drNo}</td>
				<td>${doctorVO.drName}</td>
				<td>${doctorVO.drExp}</td>
				<td>${doctorVO.drSex}</td>
				<td><img src="DBGifReader2?drNo=${doctorVO.drNo}"></td>
				<td>${doctorVO.drBirth}</td>
				<td>${doctorVO.drAdd}</td>
				<td>${doctorVO.drTel}</td>
				<td>
					<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/dr/dr.do">
						<input type="submit" value="修改">
						<input type="hidden" name="drNo" value="${doctorVO.drNo}"> 
						<input type="hidden" name="requestURL"value="<%=request.getServletPath()%>">
						<input type="hidden" name="whichPage" value="<%=whichPage%>">
						<input type="hidden" name="action" value="getOne_For_Update">
					</FORM>
				</td>
				<td>
					<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/dr/dr.do">
						 <input type="submit" value="刪除"> 
						 <input type="hidden" name="drNo" value="${doctorVO.drNo}"> 
						 <input type="hidden" name="requestURL" value="<%=request.getServletPath()%>">
						 <input type="hidden" name="whichPage" value="<%=whichPage%>">
						 <input type="hidden" name="action" value="delete">
					</FORM>
				</td>
			</tr>
		</c:forEach>
	</table>
	<%@ include file="pages/page2.file"%>

	<br>本網頁的路徑:
	<br>
	<b> <font color=blue>request.getServletPath():</font> <%=request.getServletPath()%><br>
		<font color=blue>request.getRequestURI(): </font> <%=request.getRequestURI()%>
	</b>
<!-- jsp結束 -->
    
  </c:if>  
<%@ include file="/menu2.jsp" %> 



</body>
</html>
